/** Подключение модулей и реекспорт в приложение здесь */
import Handlebars from './handlebars-v4.0.11';
import page from './page';
import debug from './debug';

//set debug options
global.localStorage.debug = '*';
var log = debug('log');
var warn = debug('warn');
var notice = debug('notice');


export { Handlebars, page, log, warn, notice };