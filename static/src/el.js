'use strict';
/** El - our small UI lib */

import { Handlebars as Hb, log, notice, warn } from '../vendors';

var __scope = __scope || {};

var bindings = fetchElements();

document.addEventListener('DOMContentLoaded', function () {
  bindings.map(function (b) {
    bind(b);
  });
});


/** Собирает все "data-bind" в стек для заданного узла (подразумевается это "страница")
 * @param {HtmlNode}
 * @return {Array} Element bindings array [ {elem, bind, template} ]
**/
function fetchElements(node) {
  // если нода не задана явно - то анализируем документ целиком
  if (!node) {
    node = document;
  }

  var elements = [],
      bindName, tpl;

  var nodeList = node.querySelectorAll('[data-bind]');

  nodeList.forEach(function (node) {
    // Bind to
    bindName = node.getAttribute('data-bind');

    // template
    tpl = Hb.compile(node.innerHTML);

    // массив где хранятся все найденные элементы
    elements.push({
      elem: node,
      bind: bindName,
      template: tpl
    });
  });

  return elements;

}


// привязывает dom-елемент с El
// Для каждого зарегистрированного El вызывает его хандлер с параметром render
function bind(b) {
  var fn = __scope[b.bind],
      clickFn;

  if (!fn) {
    console.warn('Binded fn not found in scope "window". Binded element:', b.bind);
    return;
  }

  if (typeof fn === 'function') {

    var renderedNode;

    var render = function (data) {

      if (!data) data = {};
      b.elem.innerHTML = b.template(data);
      // b.elem.style.visibility = 'visible';
      b.elem.style.display = 'block';
      /* после рендера возвращаем ссылку на ноду-контейнер*/

      setEventHandlers(b.elem);

      return b.elem;

    };

    // self - область видимости класса компонента
    var self = fn(render);


    function setEventHandlers(node) {


      // обработка событий  клика - находим эти события и перевешиваем их
      // вызываем методы указанного элемента
      var clickNodes = [];

      if (node.hasAttribute('data-click')) clickNodes.push(node);
      clickNodes.push(...node.querySelectorAll('[data-click]'));

      clickNodes.forEach(function (n) {

        n.addEventListener('click', function (e) {
          clickFn = n.getAttribute('data-click');
          if (clickFn && self[clickFn]) {
            return self[clickFn](e);
          }
          notice('click function not found - ', clickFn);
        });
      });
    }
  }

}


/*
* @bindName - data-bind параметр
* @handler - обработчик компонента
* вызывать через NEW!
*
* */

function El(bindName, handler) {

  var self = this;

  /* проверить что вызыван как класс */
  if (!new.target) {
    throw new Error('Called without NEW.');
  }

  if (!bindName) {
    throw new Error('"bindName" parameter not exits.');
  }

  this.bindName = bindName;

  if (__scope[bindName]) {
    throw new Error('The bindName "' + bindName + '" already taken.');
  }

  /**
   * регистрируем выхов хандлера для нашего элемента
   * передаем self - ханделeр иметь общий this с классом El
   */
  __scope[bindName] = function (render) {
    handler.call(self, render);
    return self;
  };
};

export default El;