'use strict';
baguetteBox.run('.h-baguette-gallery');

async function qtyArrowHandler(e, row_id) {
  let direction = e.target.getAttribute('data-qty-direction'),
      addQty    = 0;

  if (direction === 'down') {
      --addQty;
  } else if (direction === 'up') {
      ++addQty;
  } else {
      console.log('warn, wrong qty-arrow-direction value or it doesnt exists: ', direction);
      return;
  }
  let qtyInput = e.target.parentElement.querySelector('input'),
      qty      = parseInt(qtyInput.value, 10) || 1;

  qty = ((qty + addQty) < 1) ? 1 : (qty + addQty);

  qtyInput.value = qty;

  qtyInput.onchange && qtyInput.onchange();

  if (row_id) {
      Cart.updateRowQty(row_id, qty);
  };
}

window.addEventListener('load', () => {
    setTimeout(() => {
        Array.from(document.querySelectorAll('.js-qnt-input')).map(input => {
            input.addEventListener('change', e => {
                const qtyInput =  e.target,
                      row_id   = parseInt(qtyInput.getAttribute('data-row-id')),
                      qty      = parseInt(qtyInput.value, 10) || 1;

            Cart.updateRowQty(row_id, qty);
            });
        });
    }, 500);
});

window.addEventListener('load', () => {
    Array.from(document.querySelectorAll('.nav-tab li a')).map(nav => {
        nav.addEventListener('click', e => {
            e.preventDefault();

            const item         = e.target.closest('.nav-tab li'),
                  itemPosition = item.getAttribute('data-class'),
                  tab          = document.querySelector('.tab-panel_' + itemPosition);

            Array.from(document.querySelectorAll('.tab-panel')).map(i => {
                i.classList.remove('active');
            });

            Array.from(document.querySelectorAll('.nav-tab li')).map(i => {
                i.classList.remove('active');
            });

            tab.classList.add('active');
            item.classList.add('active');

        });
    })
});

const Cart = {
    
    add: async function addToCart(event, itemId, quantity) {
        const { product_id } = await toCart(itemId, quantity),
             button = event.target;

        if (product_id != itemId) {
            button.textContent = 'Не добавлено';
            return;
        };

        button.textContent = 'Добавлено';
        location.reload();
    },

    remove: async function(event, cart_row_id) {
        const removedRowId = await removeFromCart(cart_row_id);

        if (cart_row_id == removedRowId) {
            location.reload();
        };
    },
    
    updateRowQty: async function(row_id, qty) {
        await new cartQuantity(row_id, qty)
        location.reload();
    },

    makePurchase: function(event) {
        event.preventDefault(); 
        const name     = document.getElementById('userName'),
              phone    = document.getElementById('userPhone'),
              comment  = document.getElementById('userComment'),
              delivery = document.getElementById('delivery');

        window.location = `/cart/checkout/?name=${name ? name.value : ''}&phone=${phone ? phone.value : ''}&comment=${comment ? comment.value : ''}&delivery=${delivery ? parseInt(delivery.value) : ''}`;
    },
};

const Account = {
    save: async function (e) {
        e.preventDefault();

        const data = {
            name            : document.getElementById('account-name').value,
            email           : document.getElementById('account-email').value,
            phone           : document.getElementById('account-phone').value,
        }
    
        const updated = await saveAccountInfo(data);

        if (!updated) {
            alert('Данные не сохранены');
            return;
        }
    },

    changePassword: async function (e) {
        e.preventDefault();

        const pass           = document.getElementById('userPassword').value,
              newPass        = document.getElementById('userNewPassword').value,
              newPassConfirm = document.getElementById('userNewPasswordConfirmation').value,
              errorDiv       = document.querySelector('.js-form-error'),
              successDiv     = document.querySelector('.js-form-success');


        if (pass.length < 6) {
            errorHandler(errorDiv, 'Пароль должен быть как минимум из 6 символов');
            return;
        }

        if (newPass !== newPassConfirm) {
            errorHandler(errorDiv, 'Повторите ваш новый пароль');
            return;
        }

        if (pass === newPass) {
            errorHandler(errorDiv, 'Ваш новый пароль не отличается от старого');
            return;
        }
        
        const { error, response } = await saveNewPw({oldPass: pass, newPass: newPass});

        if (error) {
            errorHandler(errorDiv, error)
            return;
        }

        successHandler(successDiv, response);
        document.getElementById('rf-account-form').reset();
    },
}

function errorHandler(div, error) {
    div.textContent = error;
    div.hidden = false;
    return;
}

function successHandler(div, success) {
    div.textContent = success;
    div.hidden = false;
    return;
}