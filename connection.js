'use strict';

module.exports = {
  database:         'dev',
  host:             'localhost',
  user:             'dev',
  password:         '111111',
  port: 5432,
  max:              20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
};
