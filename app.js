'use strict';

const app          = require('core'),
      account      = require('account'),
      cart         = require('./modules/cart'),
      main         = require('./modules/main');
      // avangard     = require('./modules/avangard'),

app.registerModule(account, cart, main);
app.start({
  port: 3001,
  host: '0.0.0.0'
});
