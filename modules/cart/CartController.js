'use strict';
const Models  = require('./models');

class Cart {
  constructor() {
    this.init();
  };

  init() {};
  
  /**
   * @param {number} itemId 
   * @param {{id: number, user: {}}} session 
   * @description Если не найдена сессия – создает сессию и корзину, 
   *              добавляет товары в эту корзину;
   * @description Если корзина найдена для сессии, просто добавляет товары в корзину;
   */
  async add(itemId, quantity, session) {
    let cartId = await Models.checkCartBySession(session.id);

    if (!cartId) {
      const dbSession = await Models.createSession(session);

      cartId = await Models.createCart(dbSession);
    };

    return await Models.addItemToRow(itemId, cartId, quantity)
  };

  /**
   * 
   * @param {number} cart_row_id 
   * @param {string} app_session 
   */
  async remove(cart_row_id, app_session) {
    return await Models.deleteItemFromRow(cart_row_id, app_session);
  }

  async updateQuantity(row_id, quantity, app_session) {
    return await Models.updateRowQuantity(row_id, quantity, app_session);
  }

};

module.exports = Cart;