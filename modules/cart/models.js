
const { Items } = require('../main/entity/ItemEntity'),
      db        = $global.db;

class Cart extends $global.Table {
  constructor() {
    super('t_cart c');
    this.select('*');
  };
};

class Order extends $global.Table {
  constructor() {
    super('t_order o')
    this.select('*');
  };
};

class Orderline extends $global.Table {
  constructor() {
    super('t_orderline o')
    this.select('*');
  };
};

class CartEntity extends $global.Table {
  constructor() {
      super('t_cart c');
      this.
        select(
          'cr.product_id as item_id',

          'c.session_id',

          'cr.fixed_price',

          'c.is_ckeckouted',

          'cr.maker',

          'cr.cart_id',

          'cr.article',

          'cr.caption',

          'cr.image_url',

          'cr.cart_row_id',

          'cr.quantity',

          `(select pv.value from t_item_prop ip left join t_prop_value pv on pv.prop_value_id = ip.prop_value_id where pv.prop_id = 68 and ip.item_id = cr.product_id) as type `,

          `(select json_agg(t1) from(
            SELECT DISTINCT p.name, p.brief, tpv.value 
            FROM t_item_prop ip
              left join t_prop_value tpv on tpv.prop_value_id = ip.prop_value_id
              left join t_prop p on p.prop_id = tpv.prop_id
            WHERE ip.item_id = cr.product_id
          ) t1) props`,
        )

      .left('t_cart_row cr').on('cr.cart_id', 'c.cart_id')
  }
};

class CartRow extends $global.Table {
  constructor() {
    super('t_cart_row cr');
    this.select('*');
  }
};

class SessionEntity extends $global.Table {
  constructor() {
    super('t_session s');
    this.select('*');
  }
};

/**
 * @param {number} app_session 
 * @description Принимает сессию экспреса и ищет по ней сессию в дб;
 */
async function getSessionId(app_session) {
  const [ session ] = await new SessionEntity().filter('app_session').eq(app_session).commit();

  if (!session) {
    return null;
  }

  return session.session_id;
};

/**
 * 
 * @param {string} app_session 
 * @description Проверяет есть ли уже такая сессия(сессия экспресса в таблице t_session);
 * @description Если сессия найдена, возвращает ID корзины с помощью @function getCartBySession;
 */
async function checkCartBySession(app_session) {
  const [ session ] = await new SessionEntity().filter('app_session').eq(app_session).commit();

  if (!session) {
    return null;
  }
  
  return await getCartBySession(session);
}

/**
 * @param {number} session_id 
 * @description Ищет корзину по параметру session_id;
 */
async function getCartBySession(session) {
  const [ cart ] = await new Cart().filter('session_id').eq(session.session_id).filter('is_ckeckouted').neq(true).commit();
  
  if (!cart) {
    return await createCart(session);
  }
  
  return cart.cart_id;
}

/**
 * @param {{ id: number, user: {user_id: number} }} session;
 * @description Записывает сессию
 * @returns session_id
 */
async function createSession(session) {
  const GUEST_USER  = 694;
  const [ insert ]  = await new SessionEntity()
    .insert({
       app_session : session.id, 
       user_id     : session.user ? session.user.user_id : GUEST_USER
      });

  if (!insert) {
    return null;
  }
  
  return insert;
}
/**
 * @param {string} app_session сессия экспресса; 
 */
async function getCart(app_session) {
  const dbSession = await getSessionId(app_session);
  return await new CartEntity().filter('session_id').eq(dbSession).filter('is_ckeckouted').neq(true).commit();
}

/**
 * @param {string} app_session сессия экспресса; 
 */
async function _getCart(app_session) {
  const dbSession = await getSessionId(app_session);
  return await new CartEntity().filter('session_id').eq(dbSession).commit();
}

/**
 * @param {number} itemId 
 * @returns {[{item_id: number, article: string, brief: string, item_name: string, pl_price: number, manuf_name: string, image_url: string, pl_price: number}]}
 * @description Отдает всю необходимую информацию для того, чтоб добавить товар в корзину; 
 */
async function itemInfoById(itemId) {
  return await new Items().filter('item_id').eq(itemId).commit();
}

/**
 * @param {number} itemId 
 * @param {number} cartId 
 * @description  Добавляет товар в корзину;
 */
async function addItemToRow(itemId, cartId, quantity) {
  const [ item ] = await itemInfoById(itemId);

  if (!item) {
    return;
  }
  
  const [ row ] = await new CartRow()
    .insert({
      cart_id       : cartId,
      product_id    : item.item_id,
      fixed_price   : item.pl_price,
      current_price : item.pl_price,
      quantity      : quantity,
      product_type  : 0,
      caption       : item.item_name,
      maker         : item.manuf_name,
      article       : item.article,
      image_url     : item.image_url,
      item_brief    : item.brief
    });
    
    return row;
};

/**
 * 
 * @param {string} _cart_row_id 
 * @param {string} app_session
 * @description проверяем, принадлежит ли карт сессии, удаляем позицию из корзины; 
 */
async function deleteItemFromRow(_cart_row_id, app_session) {
  const [ cart ]         = await getCart(app_session);

  // Удаленая позиция.
  const { cart_row_id } = await db.sql(`
    DELETE FROM 
      t_cart_row
    WHERE cart_row_id = ${_cart_row_id} and 
          cart_id     = ${cart.cart_id}
    RETURNING cart_row_id;
  `);

  if (_cart_row_id != cart_row_id) {
    return null;
  }

  return cart_row_id
};

/**
 * 
 * @param {number} cart_row_id 
 * @param {number} quantity 
 * @param {number} app_session 
 */
async function updateRowQuantity(cart_row_id, quantity, app_session) {
  const [ row ] = await new CartRow().update(cart_row_id, ['quantity', 'provider'], [quantity, 0]) 

  if (!row && quantity != row.quantity) {
    return null;
  }

  return row.quantity;
};

async function createCart(session) {
  const [ insert ] = await new CartEntity().insert({
    session_id : session.session_id,
    user_id    : session.user_id,
  });

  if (!insert) {
    return null;
  }

  return insert.cart_id
};
    
/**
 * 
 * @param {{}} session 
 * @param {{phone: string, orderComment: string}} opts
 * @description ищет по сессии корзину,  создает по ней заказ;
 */
async function createOrder(session, opts) {

  const dbSession = await getSessionId(session.id);

  if (!dbSession) {
    throw new Error('Сессия не была найдена в базе');
  }

  const [ cart ]  = await new Cart().filter('session_id').eq(dbSession).filter('is_ckeckouted').neq(true).commit();

  if (!cart) {
    throw new Error('Корзина не была найдена', cart);
  }

  const [ order ] = await new Order().insert({
    cart_id     : cart.cart_id,
    user_id     : cart.user_id,
    insert_date : 'now',
    comment     : `${opts.name ? 'Имя: ' + opts.name : ''}${opts.phone ? ', телефон: ' + opts.phone : ''}${opts.comment ? ', коммент:' + opts.comment : ''}`,
    partner_id  : Object.values(await db.sql(`select subject_id from t_user where user_id = ${cart.user_id}`))[0],
    status_id   : 1,
  });


  if (!order) {
    throw new Error('Заказ не был создан', order);
  }

  return {
    order         : order,
    deliveryPrice : await addDeliveryPriceInOrderline(order.order_id, opts.deliveryPrice),
    lines         : await createOrderlines(order.order_id, cart.cart_id),
  }
};

/**
 * @description создает товар(Стоимость доставки);
 */
async function addDeliveryPriceInOrderline(order_id, deliveryPrice) {
  return new Orderline().insert({
    order_id      : order_id,
    price         : deliveryPrice,
    caption       : 'Доставка',
    cart_quantity : 1,
  });
}

/**
 * @param {number} order_id;
 * @param {number} cart_id;
 * @description на основе t_cart_row  создает t_orderline,  привязывает к заказу;
 */
async function createOrderlines(order_id, cart_id) {
  const myRows = await new CartRow().filter('cart_id').eq(cart_id).commit();

  if (!myRows) {
    throw new Error('Позиции корзины не найдены', myRows);
  }
  
  const rowsToInsert = myRows.map(row => {
    return new Orderline().insert({
      order_id       : order_id,
      product_id     : row.product_id,
      comment        : row.comment,
      caption        : row.caption,
      price          : row.current_price,
      purchase_price : row.purchase_price,
      cart_quantity  : row.quantity,
      description    : row.comment,
    });
  })


  let concatedRows = [];

  for (let row of await Promise.all(rowsToInsert)) {
     concatedRows.push(...row);
  }

  if (myRows.length !== concatedRows.length) {
    throw new Error('Количество инсертов в t_orderline не равно количеству позиций в корзине');
  }

  await checkoutCart(cart_id);

  return concatedRows;
}



/**
 * 
 * @param {number} cart_id 
 * @todo убрать второй параметр из апдейта;
 * @description переводит корзину в оформленую;
 */
async function checkoutCart(cart_id) {
  return await new Cart().update(cart_id, ['is_ckeckouted', 'checkouted'], [true, '2018-07-24 11:27:36.267634+00'])
};

/**
 * @param {string} session 
 */
async function checkOut(session, opts) {
 
  try {

    const order = await createOrder(session, opts);
    
    order.order.totalSum = order.lines.reduce((sum, current) => sum + (current.price * current.cart_quantity), 0);

    return order;

  } catch(err) {

    return debug(err);

  }
}

/**
 * @param {string} app_session
 * @description ищет заказ(ы) по сессии, возвращает заказ(ы); 
 */
async function findOrderBySession(app_session) {
  const carts  = await new Cart().filter('session_id').eq(await getSessionId(app_session)).filter('is_ckeckouted').eq(true).commit();
 
  if (!carts) {
    return;
  }

  const cartIds = carts.map(cart => cart.cart_id),
        orderPromises = cartIds.map(cartId => new Order().filter('cart_id').eq(cartId).commit());

  const concatedOrders = [];
  for (let order of await Promise.all(orderPromises)) {
      concatedOrders.push(...order);
  }

  return concatedOrders;
};

/**
 * 
 * @param {{}} session
 * @description принимает экспресс сессию, ищет ордеры по этой сессии, и если находит, то апдейдит user_id; 
 */
async function linkOrdersBySessionToUser(session) {
  const orders   = await findOrderBySession(session.id),
        orderIds = orders.map(order => order.order_id);

  orderIds.map(async orderId => await new Order().update(orderId, ['user_id', 'order_type_id'], [session.user.user_id, 1]))
}

module.exports = {
  CartRow,
  getCart,
  _getCart,
  CartEntity,
  SessionEntity,
  createSession,
  createCart,
  addItemToRow,
  deleteItemFromRow,
  updateRowQuantity,
  checkCartBySession,
  getSessionId,
  checkOut,
  linkOrdersBySessionToUser,
  findOrderBySession
};
