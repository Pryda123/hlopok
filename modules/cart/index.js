const Blocks                 = require('./blocks'),
      { findTemplate }       = require('core/find-template'),
      RetailModule           = require('core/retail-module'),
      CartController         = require('./CartController'),
      Models                 = require('./models');

      
class Cart extends RetailModule {
  init() {
    this.viewDir = __dirname + '/views';
  };

  start($app) {
    const Account = $app.getModule('AccountManager');
    Notify  = $app.getModule('Notify');

    this.blocks = Blocks;
    $app.get('/cart/', userCart);
    $app.get('/cart/checkout', checkOut);
    $app.wrap(toCart);
    $app.wrap(removeFromCart);
    $app.wrap(cartQuantity);    
    // Account events;
    Account.on('auth', session => {
      Models.linkOrdersBySessionToUser(...session);
    })
  }
}

let Notify;
const cart = new CartController();

function userCart(req, res) {
  req.app.locals.siteTitle = 'Оформление заказа';
  res.view('Checkout');
}

async function checkOut(req, res) {  
  const opts = {
    name          : req.query.name,
    phone         : req.query.phone,
    comment       : req.query.comment,
    deliveryPrice : req.query.delivery,
  }

  const order = await Models.checkOut(req.session, opts);


  Notify.toQueue({
    sender  : 'sil@kabiev.com',
    title   : 'Новый заказ',
    body    : `<div>Сумма заказа ${order.totalSum}</div>`
  });

  res.redirect(`/order/${order.order.order_id}`);
}

// Cart;
async function toCart(itemId, quantity) {
  return await cart.add(itemId, quantity, this.req.session);
};
async function removeFromCart(cart_row_id) {
  return await cart.remove(cart_row_id, this.req.session.id);
};
async function cartQuantity(row_id, quantity) {
  return await cart.updateQuantity(row_id, quantity, this.req.session.id);
};

module.exports = Cart;