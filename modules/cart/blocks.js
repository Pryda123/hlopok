'use strict';

const { DynamicBlock } = require('core/dynamic-blocks'),
      Models           = require('./models'),
      calculator       = require('../../modules/calculator/index');




 /*
 Описывает расценки на доставку по конкретному городу где: 
 type - тип(экспресс , стандарт и.т.д),
 term- срок доставки в днях, 
 delivery - цена за описанное колл-во кг,
 plus - надбавка в рублях за каждый последующий кг после 5кг. 
*/
 const city = {

  name: 'Москва',
  type: 'standart',

  term: {
    min: 2,
    max: 3
  },

  delivery: {
    1: 290, 
    2: 320,
    3: 350,
    4: 380,
    5: 410,
  },
  
  plus: 45,

};


function Blocks(req, render) {
  return {

    Show: DynamicBlock(async () => {
     return render('Cart', { 
       cart: await Models.getCart(req.session.id)
      })
    }),

    checkout: DynamicBlock(async () => {
            const cart  = await Models.getCart(req.session.id),
                  order = calculator.calculate(city,cart);
  
      
      return render('blocks/checkout', {
        user: req.session.user,
        cart: cart,
        order: order,
        orderSum: () => {
          let sum = 0;
          cart.forEach(i => sum += i.fixed_price * i.quantity);
          return Math.round(sum);
        }      
      });
    }),

  };
};

module.exports = Blocks;