'use strict';
const Blocks                 = require('./blocks'),
      { findTemplate}        = require('core/find-template'),
      RetailModule           = require('core/retail-module'),
      models                 = require('./models'),
      { Items }              = require('./entity/ItemEntity'),
      { ProductQueryFilter } = require('./filters');

class Main extends RetailModule {
  init() {
    this.viewDir = __dirname + '/views';
  };

  start($app) {
    this.blocks = Blocks;
    $app.get('/', index);
    $app.post('/task/:brief', taskQuestion);
    $app.get('/item/:id', item);
    $app.get('/cat/:brief', catByBrief);
    $app.get('/cat', cat);
    $app.get('/pages/:brief', page);
    $app.get('/order/:id', orderCheck);
    $app.get('/orders/', orders);
    $app.get('/robots.txt', robots);
    $app.get('/news', getNews);
    $app.get('/news/:brief', getNewsByBrief)
    $app.modules(); 
  }
};

function index(req, res) {
  req.app.locals.siteTitle = 'Хлопковый Рай';
  res.view(findTemplate('index.ejs'));
}

function robots(req, res) {
  const txt = 'User-agent: * Disallow: /'
  res.send(txt)
};

/**
 * @param {string} page;
 * @description ищет по req.param'у страницу через t_page;
 */
async function page(req, res) {
  const brief        = req.params.brief,
        [ pageInfo ] = await models.getPage(brief);

  if(!pageInfo) {
    return res.view(`../views/pages/${'404'}`);
  }

  req.app.locals.siteTitle = pageInfo.title;
  res.view(findTemplate(`${pageInfo.path}${pageInfo.tpl}`));
}

function cat(req, res) {
  res.view('cat');
}

async function orderCheck(req, res) {
  const user                 = req.session.user,
        orderId              = req.params.id,
        app_session          = req.session.id,
        { orderData, error } = await models.getOrderData(orderId, app_session, user);

  req.app.locals.siteTitle = `Заказ ${orderId}`;

  res.view('order', {
     orderData,
     user,
     error
    });
}


async function getNews(req, res) {

  const content = await models.getNews();

  if(content.error) {
    return res.view(findTemplate('news/news.ejs'), {
      content : null,
      error   : content.error
    });
  }

  req.app.locals.siteTitle = `Новости`;

  res.view(findTemplate('news/news.ejs'), { content });


};


async function getNewsByBrief(req, res) {

  const brief = req.params.brief,
        post  = await models.getPost(brief);
       
  if(!post) {
    req.app.locals.siteTitle = 'Ошибка';
    return res.view(findTemplate('news/post.ejs'), { post, error: 'Не удалось загрузить новость, пожалуйста обратитесь к администрации сайта!' });
    
  }

  req.app.locals.siteTitle = post.title;

  res.view(findTemplate('news/post.ejs'), { post });

}

/**
 * 
 * @description отдает список заказов;
 */
async function orders(req, res) {
  const user                 = req.session.user,
        app_session          = req.session.id,
        { orders, error}     = await models.getOrders(user);

  req.app.locals.siteTitle = `Мои заказы`;

  res.view('orders', {
    orders,
    error
  });
}

/**
 * 
 * @description Ищет категорию по брифу,
 * translateCategories = нужен для того, чтоб переводить бриф
 * @todo Переделать. Это вынужденная свистоперделка.
 */
function catByBrief(req, res) {
  const cat = req.params.brief;
  const translateCategories = {
    podushki: 'Подушки',
    satin: 'Сатин',
    byaz: 'Бязь',
    poplin: 'Поплин',
    odeyala: 'Одеяла',
    namatrasniki: 'Наматрасники',
  };
  
  req.app.locals.siteTitle = translateCategories[cat];
  res.view('cat', {
    cat
  });
};

/**
 * 
 * @description Отдает item
 * @todo рефактор item.cat_name & cat_brief, сейчас это сделано для 
 * того, чтоб хлебные крошки корректно работали; 
 */
async function item(req, res) {
  const itemId         = req.params.id,
        [ item ]       = await new Items().filter('item_id').eq(itemId).commit();
        item.cat_brief = item.cat_brief.match(/[\wа-яё]+/i).toString();
        item.cat_name  = item.cat_name.match(/[\wа-яё]+/i).toString();

  req.app.locals.siteTitle = `${item.item_name}, ${item.cat_name}`;
  res.view('item', { 
    item
  });
};

async function taskQuestion(req, res) {
  const question  = req.body,
        subjectId = req.session.user ? req.session.user.subject_id : 58;
  await models.addTask(question, subjectId);

  res.view(`../views/pages/${req.params.brief}`);      
}
module.exports = Main;