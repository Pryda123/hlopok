'use strict';
const { DynamicBlock }       = require('core/dynamic-blocks'),
      { setItems }           = require('./models'),
      { ProductQueryFilter } = require('./filters');

const concatCategories = {
  podushki      : ['podushki'],
  satin         : ['satin-zhakkard', 'satin-rotor', 'satin-elitnij'],
  byaz          : ['byaz-odnotonnaya', 'byaz-gost-145-gr', 'byaz-roskoshnaya', 'byaz-klassika130-gr'],
  poplin        : ['poplin', 'poplin-zhakkard'],
  odeyala       : ['odeyala'],
  namatrasniki  : ['namatrasniki'],
};

// TODO: DELETE, брать русское название ката из базы;
const translateCategories = {
  podushki    : 'Подушки',
  satin       : 'Сатин',
  byaz        : 'Бязь',
  poplin      : 'Поплин',
  odeyala     : 'Одеяла',
  namatrasniki: 'Наматрасники',
};

function Blocks(req, render) {
  return {

    meta: {
      title: function (title) {
        return `<title>${title ? title : 'Хлопковый рай'}</title>`
      }
    },

    header:  function () {
      const user = req.session.user;
      return render('../../../views/partials/header', {
        user
      })
    },

    footer: function () {
      return render('../../../views/partials/footer')
    },

    /**
     * @param {string} view  – путь до файла начиная с views этого модуля,
     * @param {string} brief - бриф сета из t_set;
     */
    ItemSet: DynamicBlock(async (view, brief, name) => {
      if (!view) 
        return 'Необходимо указать путь';
        
      if (!brief) 
        return 'Необходимо передать бриф сета';

      return render(view, {
        set         : await setItems(brief),
        block_brief : brief,
        block_name  : name
      }, );
    }),
    
    /**
     * @param {string} brief
     */
    Category: async function (brief) {
      if (!brief) return 'Необходимо передать бриф категории';

      const Cat   = new ProductQueryFilter(req.query),
            items = await Cat.filterIn('cat_brief', concatCategories[brief]).limit(100).commit();
      return render('blocks/catFilter', {
        form    : Cat.form,
        items   : items,
        catName : translateCategories[brief]
      });
    }
  }
};

module.exports = Blocks;