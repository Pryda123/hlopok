'use strict';

const QueryFilter                                             = require('core/query_filter'),
      { ItemsForFilter }                                      = require('./entity/ItemEntity'),
      { RangeFilter, BoolFilter, ProducerFilter, FastSearch } = require('./elements');

class ProductQueryFilter extends QueryFilter {

  constructor(q) {
    super(q);
  }

  init() {

    this.entity = new ItemsForFilter().filter('is_disabled').neq('true').filter('prototype').eq(1);
    
    const props = this.QueryParam('p', val  => {
      const test = [...val];
      const prop_brief = test[0].split('_')[0];
      const prop_value = test[0].split('_')[1];
      this.entity.filter(prop_brief).in(prop_value).filter('prototype').eq(1);
    });

    this.form = {
      props: brief => ProducerFilter(props, brief),                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    }
    
  }
}


module.exports = {
  ProductQueryFilter
};