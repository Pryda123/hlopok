'use strict';

const shortid    = require('shortid'),
      { Items }  = require('./entity/ItemEntity'),
      { Cat }    = require('./entity/CatEntity'),
      Cache      = {};


const concatCategories = {
  podushki: ['podushki'],
  satin: ['satin-zhakkard', 'satin-rotor', 'satin-elitnij'],
  byaz: ['byaz-odnotonnaya', 'byaz-gost-145-gr', 'byaz-roskoshnaya', 'byaz-klassika130-gr'],
  poplin: ['poplin', 'poplin-zhakkard'],
  odeyala: ['odeyala'],
  namatrasniki: ['namatrasniki'],
};

function FastSearch(val, title) {

  const domId = shortid.generate();

  return function () {
    return `
      <label for="${domId}">${title}</label>
      <input id="${domId}" type="text" name="${val.name}" value="${val.value || ''}" />
    `;
  };
}

function RangeFilter(val1, val2, title) {

  return function () {
    return `<div>${title}</div>
      <input type="text" name="${val1.name}"  value="${val1.value || ''}" >
      <input type="text" name="${val2.name}"  value="${val2.value || ''}" >
      `;
  };
}

function BoolFilter(param, title) {

  let domId = shortid.generate();

  return function () {
    return `
      <div class="filter-block filter__multiselect">
        <li>
          <label for="${domId}">${title}</label>
          <input id="${domId}" type="checkbox" name="${param.name}" value="${param.value || '1'}" ${param.value ? 'checked' : ''} />
        </li>
      </div>`;
  };
}

function ProducerFilter(param, cat) {
  async function filter() {
    const props = await new Cat().filter('brief').in(...concatCategories[cat]).filter('prop_id').notin(70, 71,72,73).commit();
    const filtered = props.reduce((accumulator, current) => {
      if (!accumulator.find(({
          prop_id
        }) => prop_id === current.prop_id)) {
        accumulator.push(current);
      }
      return accumulator;
    }, []);


    let el = filtered.map(producer => {
      if (producer.props_values) {

          if (param.value) {
            producer.active = param.value.some(p => {
                return p === producer.cat_id;
            });
          }
          
          const prop_values = producer.props_values.map(i => {
            if (i.prop_id == 69 && i.brief.split('-')[0] !== cat) return;
            console.log(i, cat, 'PROOOPS');
            let checkboxId = shortid.generate();
                return `
                        <li>
                          <div class="checkbox">
                            <input type="checkbox" id="${checkboxId}" value="${producer.prop_brief}_${i.id}" ${producer.active ? 'checked' : ''} name="${param.name}">
                            <label class="label-text" for="${checkboxId}">${i.value}</label>
                          </div>
                        </li>
                      `
          });
  

          let domId = shortid.generate();
          return `
            <aside class="accordion-group">
              <div class="accordion-heading">
                <h3 class="accordion-toggle">${producer.prop_name}</h3>
              </div>
              <div class="accordion-body">
                <div class="accordion-inner">
                  <ul>
                    ${prop_values.join('')}
                  </ul>
                </div>
              </div>
            </aside>
          `;
      }
    });

    return el.join('');
  };
  return filter();
}


module.exports = {
  RangeFilter,
  BoolFilter,
  ProducerFilter,
  FastSearch
};