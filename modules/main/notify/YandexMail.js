'use strict';
const nodemailer = require('nodemailer'),
      config     = require('./conf');

module.exports = function(opts) {
  const poolConfig = {
    pool : true,
    host :'smtp.yandex.ru',
    secure: true,
    auth : {
      user: config.yandexMailUser,
      pass: config.yandexMailPass
    }
  };

  const transport= nodemailer.createTransport(poolConfig);
  const mailOptions = {
    from     : config.yandexMailFrom,
    to       : opts.to,
    subject  : opts.subject,
    html     : opts.body,
  };

  transport.sendMail(mailOptions, function(err,res){
    if (err) return console.log(err);
    console.log(res.envelope);
  });
};