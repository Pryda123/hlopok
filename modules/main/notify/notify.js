'use strict';
const sendMail = require('./YandexMail');

module.exports = function () {
  let n = {};
  const fromName = 'Hlopokray';

  n.questionNotify = function (task, email) {
    sendMail({
      to: email,
      subject: 'Уведомление о регистрации вашего обращения',
      body: `
<table align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td>
                <h2>Ваше обращение зарегистрировано</h2>
                <p>
                  Спасибо за ваше обращение! Наши менеджеры свяжутся с вами в ближайшее время! Номер вашего обращения ${task.task_id}                    
                </p>
                
            </td>
        </tr>
    </tbody>
</table>

<table  align="center" bgcolor="#ffffff" border="0" cellpadding="0" cellspacing="0" width="600">
    <tbody>
        <tr>
            <td>
                <p>
                    Это письмо было отправлено автоматически.<br>
                    Если Вы считаете, что получили его по ошибке, просто проигнорируйте его.
                </p>
            </td>
        </tr>
    </tbody>
</table>
        `});
  };
  
  return n;
}();