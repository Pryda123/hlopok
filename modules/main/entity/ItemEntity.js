class Items extends $global.Table {
  constructor() {
    super('t_item i');

    this.select(

        'i.item_id as row_id',

        'i.item_id',

        'i.ext_article as article_origin',

        'i.clean_article',

        "case when i.is_ymarket = true then ''да'' else ''нет'' end as is_ymarket",

        'i.article',

        "case when i.is_disabled = true then ''нет'' else ''да'' end as is_disabled",

        "case when i.stock = 1 then ''дa'' else ''нет'' end as stock",

        'i.order_weight',

        'i.brief',

        'pv.prop_value_id',

        'c.brief as cat_brief',

        'c.name as cat_name',

        'i.name as item_name',

        'i.description',

        'pln.price as pl_price',

        'm.name as manuf_name',

        'i.manufacturer_id',

        'c.cat_id',

        'i.proto_item',

        'i.prototype',

        'dl.discount_id as discount_id',

        'ti.path as image_url',

        "concat_ws('',   '', i.name, i.article) as fast_search",

        `(select min(price) from t_item i2, t_pline pl where pl.item_id = i2.item_id and i2.proto_item= i.item_id) as price_min`,

        `(select json_object_agg(p0.brief, pv0.value)
            from t_item i0
              left join t_item i1  on i0.item_id = i1.proto_item
              left join t_item_prop ip0 on i1.item_id = ip0.item_id
              left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
              left join t_prop p0 on pv0.prop_id = p0.prop_id
            where i0.item_id = i.item_id and pv0.prop_id = 69 limit 1) as cloth`,

        `(select json_agg(t2) from(
          SELECT 
            pt.name, 
            pt.item_id,
            pp.price,
            tpv.value as prop,
            (select json_agg(t3) from(
              SELECT DISTINCT
                pt.item_id, 
                tpv2.value as prop_value, 
                p2.name as prop_name,
                ip2.prop_value_id
              FROM t_item_prop ip2 
                left join t_prop_value tpv2 on tpv2.prop_value_id = ip2.prop_value_id
                left join t_prop p2 on p2.prop_id = tpv2.prop_id
              WHERE ip2.item_id = pt.item_id AND tpv2.prop_id NOT IN (68, 70, 71, 72, 73)
            ) t3) sub_props
          FROM t_item pt 
          left join t_pline pp on pp.item_id = pt.item_id
          left join t_item_prop tp on tp.item_id = pt.item_id
          left join t_prop_value tpv on tpv.prop_value_id = tp.prop_value_id
          where pt.proto_item = i.item_id and tpv.prop_id = 68) t2
          ) sub_items`
      )

      .left('t_cat c').on('c.cat_id', 'i.cat_id')

      .left('t_item_prop ip').on('ip.item_id', 'i.item_id')

      .left('t_prop_value pv').on('pv.prop_value_id', 'ip.prop_value_id')

      .left('t_manufacturer m').on('i.manufacturer_id', 'm.manufacturer_id')

      .left('t_discount_link dl').on('dl.item_id', 'i.item_id')

      .left('(select * from t_pline p where p.pl_id = 1) pln').on('pln.item_id', 'i.item_id')

      .left('t_item_image ti').on('ti.item_id', 'i.item_id')

      .sort('row_id desc');
  }
}

class ItemsForFilter extends $global.Table {
  constructor() {
    super('t_item i');
    this.select(
      'i.item_id as row_id',
      'i.name as item_name',
      'i.item_id',
      'i.is_disabled',
      'i.prototype',
      'i.proto_item',
      'i.cat_id',
      'i.order_weight as priority',
      'c.brief as cat_brief',
      'ti.path as image_url',
      'pln.price',
      `(select min(price) from t_item i2, t_pline pl where pl.item_id = i2.item_id and i2.proto_item= i.item_id) as price_min`,
      `(select json_agg(t2) from(
          SELECT 
            pt.name, 
            pt.item_id,
            pp.price,
            tpv.value as prop,
            (select json_agg(t3) from(
              SELECT
                pt.item_id, 
                tpv2.value as prop_value, 
                p2.name as prop_name,
                ip2.prop_value_id
              FROM t_item_prop ip2 
                left join t_prop_value tpv2 on tpv2.prop_value_id = ip2.prop_value_id
                left join t_prop p2 on p2.prop_id = tpv2.prop_id
              WHERE ip2.item_id = pt.item_id AND tpv2.prop_id NOT IN (68, 70, 71, 72, 73)
            ) t3) sub_props
          FROM t_item pt 
          left join t_pline pp on pp.item_id = pt.item_id
          left join t_item_prop tp on tp.item_id = pt.item_id
          left join t_prop_value tpv on tpv.prop_value_id = tp.prop_value_id
          where pt.proto_item = i.item_id and tpv.prop_id = 68) t2
          ) sub_items`,
      // /////////////////////////////////////////////////// props for filter;
      /// test
      `(SELECT
         pv0.prop_value_id
            from t_item i0
              left join t_item i1  on i0.item_id = i1.proto_item
              left join t_item_prop ip0 on i1.item_id = ip0.item_id
              left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
              left join t_prop p0 on pv0.prop_id = p0.prop_id
        WHERE i0.item_id = i.item_id and pv0.prop_id = 69 limit 1) as tiptkani`,
      
      `(SELECT 
          pv0.prop_value_id
        from t_item i0
          left join t_item i1  on i0.item_id = i1.proto_item
          left join t_item_prop ip0 on i1.item_id = ip0.item_id
          left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
          left join t_prop p0 on pv0.prop_id = p0.prop_id
        WHERE i0.item_id = i.item_id and pv0.prop_id = 68 limit 1) as razmer`,

       `(SELECT 
          pv0.prop_value_id
        from t_item i0
          left join t_item i1  on i0.item_id = i1.proto_item
          left join t_item_prop ip0 on i1.item_id = ip0.item_id
          left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
          left join t_prop p0 on pv0.prop_id = p0.prop_id
        WHERE i0.item_id = i.item_id and pv0.prop_id = 67 limit 1) as navolochka`,

       `(SELECT 
          pv0.prop_value_id
        from t_item i0
          left join t_item i1  on i0.item_id = i1.proto_item
          left join t_item_prop ip0 on i1.item_id = ip0.item_id
          left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
          left join t_prop p0 on pv0.prop_id = p0.prop_id
        WHERE i0.item_id = i.item_id and pv0.prop_id = 66 limit 1) as prostynya`,

       `(SELECT 
          pv0.prop_value_id
        from t_item i0
          left join t_item i1  on i0.item_id = i1.proto_item
          left join t_item_prop ip0 on i1.item_id = ip0.item_id
          left join t_prop_value pv0 on pv0.prop_value_id = ip0.prop_value_id
          left join t_prop p0 on pv0.prop_id = p0.prop_id
        WHERE i0.item_id = i.item_id and pv0.prop_id = 65 limit 1) as pododeialnik`,

        

    )
    .left('t_cat c').on('c.cat_id', 'i.cat_id')
    .left('t_item_image ti').on('ti.item_id', 'i.item_id')
    .left('(select * from t_pline p where p.pl_id = 1) pln').on('pln.item_id', 'i.item_id')
    .sort('priority desc');

  }
};

class ProtoItems extends Items {
  constructor() {
    super();
    this.select('*')
      .filter('t.prototype').eq(1);
  };
};

class SubItems extends Items {
  constructor(protoItem) {
    super();
    this.select('*')
      .filter('t.prototype').eq(2)
      .filter('t.proto_item').eq(protoItem);
  };
};

class SetItems extends $global.Table {
  constructor() {
    super('t_set_items si');

    this.select(

      'si.set_items_id',

      'si.set_id',

      'si.item_id',

      'ts.brief',

      'i.name as item_name',

      'm.name as manufacturer_name',

      'p.price',

      'd.discount_qty',

      'c.name as cat_name',

      'ti.path as image_path',

      `(select min(price) from t_item i2, t_pline pl where pl.item_id = i2.item_id and i2.proto_item= i.item_id) as price_min`,

      `(select json_agg(t2) from(
        SELECT 
          pt.name, 
          pt.item_id,
          pp.price,
          tpv.value as prop

        FROM t_item pt 
        left join t_pline pp on pp.item_id = pt.item_id
        left join t_item_prop tp on tp.item_id = pt.item_id
        left join t_prop_value tpv on tpv.prop_value_id = tp.prop_value_id
        where pt.proto_item = i.item_id and tpv.prop_id = 68) t2) sub_items`

    )
      .left('t_item i').on('i.item_id', 'si.item_id')
      .left('t_set ts').on('ts.set_id', 'si.set_id')
      .left('t_item_image ti').on('ti.item_id', 'i.item_id')
      .left('t_manufacturer m').on('i.manufacturer_id', 'm.manufacturer_id')
      .left('t_pline p').on('i.item_id', 'p.item_id')
      .left('t_cat c').on('i.cat_id', 'c.cat_id')
      .left('t_discount_link dl').on('i.item_id', 'dl.item_id')
      .left('t_discount d').on('d.discount_id', 'dl.discount_id')

      .sort('set_items_id desc');
  }

};

module.exports = {
  Items,
  SetItems,
  ItemsForFilter
};