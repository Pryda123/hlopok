'use strict';



class PaymentOrder extends $global.Table {
  constructor() {
    super('t_payment_order po');

    this.select(
      
      'po.payment_order_id',

      'po.payer',

      'po.recipent',

      'po.operator',

      'po.qty',

      'po.qty_plan',

      'po.qty_fact',

      'po.qty_fee',

      'po.qty_fee_2',

      'po.qty_fee_3',

      'po.qty_fee_4',

      'po.currency_id',

      'po.pay_type',

      'po.is_confirmed',

      'po.created',

      'po.confirmed',

      'po.op_name',

      'po.op_extra',

      'po.user_id',

      'po.order_number',

      'po.order_id'
    );
  };
}


module.exports = PaymentOrder;