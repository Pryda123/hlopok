class Cat extends $global.Table {
  constructor() {
    super("t_cat_prop cp");

    this.select(
        "cp.cat_prop_id as row_id",

        "cp.cat_prop_id",

        "cp.cat_id",

        "p.prop_id",

        "p.name as prop_name",
        
        "p.brief as prop_brief",

        "c.name as cat_name",

        "c.brief",

         "(select json_agg(t2) from(select pv.prop_value_id id, pv.value , pv.brief, p.prop_id  from t_prop_value pv where pv.prop_id = p.prop_id) t2) props_values"

      )
      .left("t_prop p").on("p.prop_id", "cp.prop_id")
      .left("t_cat c").on("c.cat_id", "cp.cat_id")
      .sort("row_id desc");
  }

  delete(itemID) {
    console.log(itemID, 'from entity');
    return this.remove(itemID);
  }
}

module.exports = {
  Cat,
};
