'use strict';


class Pages extends $global.Table {
  constructor() {
    super('t_page p');

    this.select (
      'p.brief',

      'p.title',

      'p.path',

      'p.tpl'
    )
  };
}



module.exports = Pages;


