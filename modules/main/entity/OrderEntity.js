'use strict';

class Order extends $global.Table {
  constructor() {
    super('t_order o');

    this.select(
      '*',
      `(select sum(ol.cart_quantity * ol.price) from t_orderline ol where ol.order_id = o.order_id and ol.deleted != true) as sum`,
      `(select json_agg(t1) from (
        
        SELECT ol.caption, 
               ol.cart_quantity, 
               ol.product_id,
               (SELECT proto_item from t_item i WHERE prototype = 2 and i.item_id = ol.product_id) 

        FROM t_orderline ol 

        WHERE ol.order_id = o.order_id and 
              ol.deleted != true

            ) t1 
        ) as goods`
    )
  }
}


module.exports = Order;