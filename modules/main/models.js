'use strict';
const { SetItems }         = require('./entity/ItemEntity'),
      Order                = require('./entity/OrderEntity'),
      Page                 = require('./entity/PageEntity'),
      notify               = require('./notify/notify'),
      db                   = $global.db;

const cartModels = require('../cart/models');

async function addTask(question, subjectId) {
  const query =`
  INSERT INTO t_task(type_id, name, status_id, description, deadline, owner_id)
    SELECT tt.task_type_id,
    (SELECT name from t_subject where subject_id = $4),
    ts.task_status_id, $2, current_date, $3
    FROM t_task_type tt
    LEFT JOIN  t_task_status ts ON tt.task_type_id = ts.task_type_id
    WHERE tt.brief = $1
    ORDER BY ts.task_status_id ASC
    LIMIT 1
  RETURNING task_id, description; 
  `;

  try {

    const taskType = 'PRICE',
      concatTask = `Сообщение: ${question.text} \nТел.: ${question.phone} \nИмя: ${question.name} \nЭл.почта: ${question.email}`,
      task = await db.sql(query, [taskType, concatTask, subjectId, subjectId]);
    notify.questionNotify(task, question.email);
    return;

  } catch (err) {
    debug(err);
    return;
  }

}

async function getPage(brief) {
  try {
    return await new Page().limit(1).filter('brief').eq(brief).commit();;
  } catch (err) {
    return false;
  }
}



async function getNews() {
  
  try {
    const data    =  await db.sql(`select * from o_content('get_list', '{}')`),
          content = [...data.res];
    return content;

  } catch(err) {

    debug(err);

    return {error: 'Не удалось загрузить новости, пожалуйста обратитесть к администрации сайта!!'};
  }

}

async function getPost(brief) {
  try {
    const { output } = await db.sql(`select * from content_by_brief($1)`, [brief]);

    return JSON.parse(output);

  } catch(err) {

    debug(err);
    
    return false;
  }
}

/**
 * 
 * @param {string} app_session 
 * @param {{}} user
 * @description ищет по сессии, если не находит, ищет по user_id; 
 */
async function getOrderData(orderId, app_session, user) {
  const [ cartBySession  ] = await cartModels._getCart(app_session);
  
  if (!cartBySession && !user) {
    return { error: 'Не найден заказ' }
  }
  
  let orderByUserId;
  let orderBySession;
  
  if (cartBySession) {
    [ orderBySession ] = await new Order().limit(1).filter('order_id').eq(orderId).filter('cart_id').eq(cartBySession.cart_id).commit();
  }

  if (!orderBySession) {
    [ orderByUserId ] = await new Order().limit(1).filter('order_id').eq(orderId).filter('user_id').eq(user.user_id).commit();
  }

  return {
    orderData: orderBySession ? orderBySession : orderByUserId
  }
}

async function getOrders(user) {
  if (!user) {
    return { error: '403' };
  }
  
  const orders = await new Order().filter('user_id').eq(user.user_id).limit(50).commit();

  if (!orders) {
    return { error: 'Нет заказов' };
  }

  return {
    orders: orders
  };
}

async function setItems(brief) {
  return await new SetItems().limit(100).filter('brief').eq(brief).commit();;
}

module.exports = {
  setItems,
  getOrderData,
  addTask,
  getOrders,
  getPage,
  getNews,
  getPost
};