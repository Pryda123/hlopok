'use strict';

const RetailModule = require('core/retail-module'),
      Models       = require('./models');

     
class Avangard extends RetailModule {
  init() {
    this.viewDir = __dirname + '/views';
  };

  start($app) {
    $app.get('/payment/:orderId', prepare);
    $app.post('/online-payment/onSuccessAVD', update);
    $app.get('/payment/result/sucess', onSuccess);
    $app.get('/payment/result/fail', onFail);
    
    
  }
}


function onFail(req, res) {
  res.view('failPayment');
}

function onSuccess(req, res) {
  res.view('successPayment');
}


async function update(req, res) {
  if(!req.body || !req.body.order_number) {
    return res.end();
  }

  const paymentResponse =  {

    paymentOrderId : +req.body.order_number,
    payType        : 1,
    paymentAmount  : req.body.amount / 100,
    tid            : req.body.id,

  };
 
  await Models.successOrder(paymentResponse);

  res.status(202).send('accepted!');

}

async function prepare(req, res) {

  const params = await Models.prepareOrder(req.params.orderId, req.session);

  res.view('payment', { params });
}


module.exports = Avangard;