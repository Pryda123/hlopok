'use strict';

const crypto = require('crypto');

const md5 = string => crypto.createHash('md5').update(string).digest('hex');

function createSignatureHash(params, credentials) {
  
  let paramsConcatString = `${params.shopId.toString()}${params.orderNumber}${params.amount.toString()}`;

  return md5(`${md5(credentials.shopSign)}${md5(paramsConcatString)}`.toUpperCase()).toUpperCase();


}


module.exports = {  
  createSignatureHash
}