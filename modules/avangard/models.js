'use strict';

const db                      = $global.db,
      { credentials }         = require('./conf'),
      { createSignatureHash } = require('./helpers'),
      OrderEntity             = require('../main/entity/OrderEntity'),
      PaymentOrderEntity      = require('../main/entity/PaymentOrderEntity'),
      order                   = new OrderEntity(),
      payment                 = new PaymentOrderEntity();



async function successOrder(data) {
  try {

   return await payment.update(
      
      data.paymentOrderId,

      ['is_confirmed', 'confirmed', 'qty', 'qty_fact', 'pay_type', 'op_extra', 'op_id'],

      [1, 'now', data.paymentAmount, data.paymentAmount, 1, `${JSON.stringify(data)}`, data.tid]
    );

  } catch(err) {

    debug(err);
  }

}


async function newOrder(appSession, params) {
  
  let operation = 'new';

  try{
    
    return await db.sql('select * from payment_order($1::text,$2::text,$3::json)',[appSession, operation, params]);

  } catch(err) {

    debug(err); 
  }
  
}


async function getUser(session) {
  
  try{

    return await db.sql(`select 0 as retval, * from (select name, email, phone from t_user where user_id = ($1)) as u`, [session.user.user_id]);

  } catch(err) {

    debug(err);

    return null;
  }
    

}


async function prepareOrder(orderId, session) {


  

  let params = {
    seoTitle         : `Форма оплаты заказа - Магазин Хлопковый рай`,
    clientName       : '',
    clientPhone      : '',
    clientEmail      : '',
    shopId           : credentials.shopId,
    shopSign         : credentials.shopSign,
    signature        : '',
    language         : credentials.language,
    url              : credentials.uri,
    backUrl          : credentials.backUrl,
    backUrlOk        : credentials.backUrlOk,
    backUrlFail      : credentials.backUrlFail,
    amount           : 0,
    orderNumber      : orderId,
    userAuthorize    : session.user ? true: false ,
    orderDescription : ''
  };


  

  try {

    const user = await getUser(session);
    params.clientName  = user ? user.name  : '';
    params.clientPhone = user ? user.phone : '';
    params.clientEmail = user ? user.email : '';

    const [ orderData ] = await order.limit(1).filter('order_id').eq(orderId).commit();

    params.amount           = Math.floor(orderData.sum) * 100;

    params.orderDescription = orderData.order_id.toString(); 

    const paymentOrder = await newOrder(session.id, { qty: params.amount / 100, orderId: params.orderNumber });

    params.orderNumber = paymentOrder.order_number;
    params.signature   = createSignatureHash(params, credentials);
    return params;

  } catch(err) {

    debug(err);
  }

}

module.exports = {
  prepareOrder,
  successOrder
}
