'use strict';


const trimString = string => string ? string.replace(/[^\.0-9]/g, '') : string;



const calculator = {

  /**
   * @param {string} length - Самый большой показатель длины среди предметов в корзине
   * @param {number} width - Самый большой показатель ширины среди предметов в корзине
   * @param {string} height - Самый большой показатель высоты среди предметов в корзине
   * @returns {number} Объемный вес.
   * @description Вычисляет объемный вес предмета основываясь на максимальных показателях переданых в параметрах
   */

  calculateVolumeWeight: (length, width, height) => Math.ceil((length * width * height) / 5000),


  /**
   * @param {object} city - Объект с данными по стоймости доставки в указаный город.
   * @param {array} goods - Корзина клиента
   * @returns {number} Сумма заказа
   * @description Возвращает сумму заказа если вес не превышает 5кг при другом условии возращает сумму с надбавкой за каждый доп.кг.
   */

  calculate: function(city, goods) {

    const order           = this.calculateOrderProps(goods),
          volWeight       = this.calculateVolumeWeight(order.dlina, order.shirina , order.visota),
          resultWeight    = (order.ves > volWeight) ? Math.ceil(order.ves) : volWeight;

    let price = city.delivery[resultWeight];

    if(!price) {

      price = Math.max(...Object.values(city.delivery)) + (resultWeight - Math.max(...Object.keys(city.delivery))) * city.plus;

    }

    return  price;

  },

  /**
   * @param {array} goods - Корзина клиента, массив объектов 
   * @returns {{dlina: string, shirina: number, visota: string, ves: number}}
   * @description Вычисляет максимальные габариты товара из корзины , а так же общий физ.вес заказа;
   */

  calculateOrderProps: function(goods) {

    let orderData = {

      dlina   : 0,
      shirina : 0,
      visota  : 0,
      ves     : 0
    };

    goods.forEach(item => {
    
      item.props.forEach(prop => {

        prop.value = trimString(prop.value);

        if(prop.brief === 'ves') {

          prop.value = +parseFloat(prop.value).toFixed(2);
          orderData[prop.brief] += (prop.value * item.quantity);

        }

        orderData[prop.brief] < prop.value ? orderData[prop.brief] = prop.value : orderData[prop.brief];

      });

    });

    return orderData;
  },


}


module.exports = calculator;